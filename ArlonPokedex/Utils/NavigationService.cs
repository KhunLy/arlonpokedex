﻿using ArlonPokedex.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ArlonPokedex.Utils
{
    public class NavigationService : INavigationService
    {
        public void OpenTypeWindow()
        {
            Window window = new TypeWindow();
            window.Show();
        }
    }
}
