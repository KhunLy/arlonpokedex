﻿namespace ArlonPokedex.Utils
{
    public interface INavigationService
    {
        void OpenTypeWindow();
    }
}