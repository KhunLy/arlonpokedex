﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArlonPokedex.Models
{
    public class Type
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
