﻿namespace ArlonPokedex.Models
{
    public class TypeSlot
    {
        public int Slot { get; set; }
        public Type Type { get; set; }
    }
}