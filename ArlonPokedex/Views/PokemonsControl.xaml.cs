﻿using ArlonPokedex.DI;
using ArlonPokedex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArlonPokedex.Views
{
    /// <summary>
    /// Logique d'interaction pour PokemonsControl.xaml
    /// </summary>
    public partial class PokemonsControl : UserControl
    {
        public PokemonsControl()
        {
            DataContext = ServicesLocator.Container.GetService(typeof(IPokemonsViewModel));
            InitializeComponent();
        }
    }
}
