﻿using ArlonPokedex.DI;
using ArlonPokedex.Models;
using ArlonPokedex.Requesters;
using ArlonPokedex.Utils;
using ArlonPokedex.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ToolBox.MVVM.Bindable;
using ToolBox.MVVM.Commands;
using ToolBox.MVVM.Mediators;

namespace ArlonPokedex.ViewModels
{
    public class PokemonDetailsViewModel : BindableBase, IPokemonDetailsViewModel
    {
        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { SetValue(ref _Id, value); }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { SetValue(ref _Name, value); }
        }

        private decimal _Height;

        public decimal Height
        {
            get { return _Height; }
            set { SetValue(ref _Height, value); }
        }

        private decimal _Weight;

        public decimal Weight
        {
            get { return _Weight; }
            set { SetValue(ref _Weight, value); }
        }

        private string _FrontDefault;

        public string FrontDefault
        {
            get { return _FrontDefault; }
            set { SetValue(ref _FrontDefault, value); }
        }

        private string _BackDefault;

        public string BackDefault
        {
            get { return _BackDefault; }
            set { SetValue(ref _BackDefault, value); }
        }


        private string _Type1;

        public string Type1
        {
            get { return _Type1; }
            set { SetValue(ref _Type1, value); }
        }

        private string _Type2;

        public string Type2
        {
            get { return _Type2; }
            set { SetValue(ref _Type2, value); }
        }

        private string _Type1Url;
        public string Type1Url
        {
            get { return _Type1Url; }
            set { SetValue(ref _Type1Url, value); }
        }

        public RelayCommand<string> OpenTypeWinCommand { get; set; }

        private readonly IPokemonRequester _Requester;
        private readonly INavigationService _NavService;

        public PokemonDetailsViewModel(
            IPokemonRequester requester, 
            INavigationService navService
        )
        {
            OpenTypeWinCommand = new RelayCommand<string>(OpenTypeWindow);
            MessagingCenter<Pokemon>.Default
                .Subscribe("SELECTED_POKEMON_CHANGED", OnPokemonChanged);
            _Requester = requester;
            _NavService = navService;
        }

        private void OpenTypeWindow(string url)
        {
            _NavService.OpenTypeWindow();
        }

        private void OnPokemonChanged(Pokemon obj)
        {
            LoadItem(obj.Url);
        }

        private async void LoadItem(string url)
        {

            PokemonDetails pkmn = await _Requester.Get(url);
            Id = pkmn.Id;
            Name = pkmn.Name;
            Height = pkmn.Height / 10M;
            Weight = pkmn.Weight / 10M;
            FrontDefault = pkmn.Sprites.FrontDefault;
            BackDefault = pkmn.Sprites.BackDefault;
            Type1 = pkmn.Types.FirstOrDefault((ts) => ts.Slot == 1)
                ?.Type.Name;

            Type2 = pkmn.Types.FirstOrDefault((ts) => ts.Slot == 2)
                ?.Type.Name;

            Type1Url = pkmn.Types.FirstOrDefault((ts) => ts.Slot == 1)
                ?.Type.Url;
        }
    }
}
