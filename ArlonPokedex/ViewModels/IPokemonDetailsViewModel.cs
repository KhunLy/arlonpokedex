﻿using ToolBox.MVVM.Commands;

namespace ArlonPokedex.ViewModels
{
    public interface IPokemonDetailsViewModel
    {
        string BackDefault { get; set; }
        string FrontDefault { get; set; }
        decimal Height { get; set; }
        int Id { get; set; }
        string Name { get; set; }
        RelayCommand<string> OpenTypeWinCommand { get; set; }
        string Type1 { get; set; }
        string Type1Url { get; set; }
        string Type2 { get; set; }
        decimal Weight { get; set; }
    }
}