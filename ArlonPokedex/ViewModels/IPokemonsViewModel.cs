﻿using System.Collections.Generic;
using ArlonPokedex.Models;
using ToolBox.MVVM.Commands;

namespace ArlonPokedex.ViewModels
{
    public interface IPokemonsViewModel
    {
        RelayCommand NextCommand { get; set; }
        string NextUrl { get; set; }
        List<Pokemon> Pokemons { get; set; }
        RelayCommand PrevCommand { get; set; }
        Pokemon SelectedPokemon { get; set; }

        void LoadItems(string url);
    }
}