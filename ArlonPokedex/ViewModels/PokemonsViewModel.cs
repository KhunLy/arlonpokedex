﻿using ArlonPokedex.Models;
using ArlonPokedex.Requesters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.Bindable;
using ToolBox.MVVM.Commands;
using ToolBox.MVVM.Mediators;

namespace ArlonPokedex.ViewModels
{
    public class PokemonsViewModel : BindableBase, IPokemonsViewModel
    {
        private string _NextUrl;
        public string NextUrl
        {
            set {
                _NextUrl = value;
                NextCommand.RaiseCanExecuteChanged(this);
            }
            get { return _NextUrl; }
        }
        private string _PrevUrl;

        private List<Pokemon> _Pokemons;

        public List<Pokemon> Pokemons
        {
            get { return _Pokemons; }
            set { SetValue(ref _Pokemons, value); }
        }

        private Pokemon _SelectedPokemon;

        public Pokemon SelectedPokemon
        {
            get { return _SelectedPokemon; }
            set
            {
                _SelectedPokemon = value;
                if(_SelectedPokemon != null)
                {
                    MessagingCenter<Pokemon>.Default
                        .Send("SELECTED_POKEMON_CHANGED", _SelectedPokemon);
                }
            }
        }


        public RelayCommand PrevCommand { get; set; }
        public RelayCommand NextCommand { get; set; }

        private readonly IPokemonRequester _Requester;

        public PokemonsViewModel(IPokemonRequester requester)
        {
            _Requester = requester;
            _Pokemons = new List<Pokemon>();
            PrevCommand = new RelayCommand(
                GoPrev, 
                () => _PrevUrl != null
            );
            NextCommand = new RelayCommand(
                GoNext,
                () => _NextUrl != null
            );
            LoadItems("https://pokeapi.co/api/v2/pokemon");
        }

        private void GoPrev()
        {
            LoadItems(_PrevUrl);
        }

        private void GoNext()
        {
            LoadItems(_NextUrl);
        }

        public async void LoadItems(string url)
        {

            PokemonsRequest req = await _Requester.GetAll(url);
            Pokemons = req.Results.ToList();
            _PrevUrl = req.Previous;
            NextUrl = req.Next;
            PrevCommand.RaiseCanExecuteChanged(this);

        }
    }
}
