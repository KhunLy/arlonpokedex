﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArlonPokedex.ViewModels;
using ArlonPokedex.Requesters;
using ArlonPokedex.Utils;

namespace ArlonPokedex.DI
{
    public class ServicesLocator
    {
        private static ServiceProvider _Container;

        public static ServiceProvider Container {

            get { return _Container = _Container ?? _Collection.BuildServiceProvider(); } }

        private static readonly IServiceCollection _Collection;

        static ServicesLocator()
        {
            _Collection = new ServiceCollection();
            RegisterServices();
            //_Container = _Collection.BuildServiceProvider();

        }

        private static void RegisterServices()
        {
            _Collection
                .AddSingleton<IPokemonDetailsViewModel, PokemonDetailsViewModel>();
            _Collection
                .AddSingleton<IPokemonsViewModel, PokemonsViewModel>();
            _Collection.AddSingleton<IPokemonRequester, PokemonRequester>();
            _Collection.AddSingleton<INavigationService, NavigationService>();
        }
    }
}
