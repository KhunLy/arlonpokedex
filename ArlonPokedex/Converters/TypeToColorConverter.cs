﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace ArlonPokedex.Converters
{
    public class TypeToColorConverter : IValueConverter
    {

        private Dictionary<string, SolidColorBrush> _Colors
            = new Dictionary<string, SolidColorBrush>
            {
                { "fire", new SolidColorBrush(Colors.Red) },
                { "water", new SolidColorBrush(Colors.LightSeaGreen) },
                { "grass", new SolidColorBrush(Colors.LightGreen) },
            };
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (_Colors.ContainsKey(value as string))
                {
                    return _Colors[value as string];
                } 
            }
            return new SolidColorBrush(Colors.White);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
