﻿using System.Threading.Tasks;
using ArlonPokedex.Models;

namespace ArlonPokedex.Requesters
{
    public interface IPokemonRequester
    {
        Task<PokemonDetails> Get(string url);
        Task<PokemonsRequest> GetAll(string url);
    }
}