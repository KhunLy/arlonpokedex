﻿using ArlonPokedex.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ArlonPokedex.Requesters
{
    public class PokemonRequester : IPokemonRequester
    {
        public async Task<PokemonsRequest> GetAll(string url)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage resp
                = await client.GetAsync(url);
            if (resp.IsSuccessStatusCode)
            {
                string json = await resp.Content.ReadAsStringAsync();
                return JsonConvert
                    .DeserializeObject<PokemonsRequest>(json);

            }
            return null;
        }

        public async Task<PokemonDetails> Get(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage resp = await client.GetAsync(url);
                if (resp.IsSuccessStatusCode)
                {
                    string json = await resp.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<PokemonDetails>(json);

                }
                return null;
            }
        }
    }
}
